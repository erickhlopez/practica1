<?php
include 'Model.php';
include 'Usuario.php';
$idUsuario=$_GET['idUsuario'];
$usuario=new Usuario();
$resultadousuario=$usuario->selectWhere("id",$idUsuario);
//guardamos los campos del resultado usuario
foreach ($resultadousuario as $fila) {
  $name=$fila['name'];
  $userName=$fila['user_name'];
  $password=$fila['password'];
  $email=$fila['email'];
}

 ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <form action="editarUsuarioBE.php" method="get" enctype="application/x-www-form-urlencoded">
      <div >
        <input type="number" name="id" value=<?php echo $idUsuario; ?> readonly>
      </div>
      <div >
        <input type="text" name="name" value=<?php echo $name; ?> required>
      </div>
      <div >
        <input type="text" name="userName" value=<?php echo $userName; ?> required>
      </div>
      <div >
        <input type="password" name="password" value=<?php echo $password; ?> required>
      </div>
      <div >
        <input type="email" name="email" value=<?php echo $email; ?> required>
      </div>
      <div >
        <input type="submit" name="btnAgregar" value="Editar Usuario">
      </div>
    </form>
  </body>
</html>
