<?php
include 'Model.php';
include 'Libro.php';

$idLibro=$_GET['idProducto'];
$libro=new Libro();
$resultadoLibro=$libro->selectWhere("id",$idLibro);
//guardamos los datos del libro
foreach ($resultadoLibro as $fila) {
  $nombre=$fila['nombre'];
  $autor=$fila['autor'];
  $editorial=$fila['editorial'];
  $genero=$fila['genero'];
  $precio=$fila['precio'];
  $descripcion=$fila['descripcion'];
}
 ?>
 <!DOCTYPE html>
 <html lang="es" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title></title>
   </head>
   <body>
     <form action="productoEditarBE.php" method="get" enctype="application/x-www-form-urlencoded">
       <div >
         <input type="number" name="id" value=<?php echo $idLibro; ?> readonly>
       </div>
       <div>
         <input type="text" name="nombre" value=<?php echo $nombre; ?> required>
       </div>
       <div >
         <input type="text" name="autor" value=<?php echo $autor; ?> required>
       </div>
       <div >
         <input type="text" name="editorial" value=<?php echo $editorial; ?> required>
       </div>
       <div >
         <input type="text" name="genero" value=<?php echo $genero; ?> required>
       </div>
       <div>
         <input type="number" name="precio" value=<?php echo $precio; ?> min="0" step="any" required>
       </div>
       <div>
         <textarea name="descripcion" rows="5" cols="40"><?php echo $descripcion; ?></textarea>
       </div>
       <div>
         <input type="submit" name="btnEnviar" value="Editar">
       </div>
     </form>
   </body>
 </html>
