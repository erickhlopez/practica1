<?php
include 'Model.php';
include 'Libro.php';

//obtenemos los valores
$id=$_GET['id'];
$nombre=$_GET['nombre'];
$autor=$_GET['autor'];
$editorial=$_GET['editorial'];
$genero=$_GET['genero'];
$precio=$_GET['precio'];
$descripcion=$_GET['descripcion'];

$libro=new Libro();
try {
  $libro->update($id,$nombre,$autor,$editorial,$genero,$precio,$descripcion);
  header('Location: index.php');
} catch (\Exception $e) {
  echo "Error al editar el libro ".$e;
}




 ?>
