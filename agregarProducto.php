<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <form action="agregarProductoBE.php" method="get" enctype="application/x-www-form-urlencoded">
      <div>
        <input type="text" name="nombre" placeholder="Nombre" required>
      </div>
      <div >
        <input type="text" name="autor" placeholder="Autor" required>
      </div>
      <div >
        <input type="text" name="editorial" placeholder="Editorial" required>
      </div>
      <div >
        <input type="text" name="genero" placeholder="Genero" required>
      </div>
      <div>
        <input type="number" name="precio" placeholder="Precio" min="0" step="any" required>
      </div>
      <div>
        <textarea name="descripcion" rows="5" cols="40">Describe tu producto...</textarea>
      </div>
      <div>
        <input type="submit" name="btnEnviar" value="Agregar">
      </div>
    </form>
  </body>
</html>
